package com.example.shareimg

import android.content.Context
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.shareimg.R
var listImages: MutableList<Uri> = mutableListOf()

class MyAdapter() : RecyclerView.Adapter<MyAdapter.MyViewHolder>() {




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val context = parent.context
        val layoutIdForListItem: Int = R.layout.rowlayout
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(layoutIdForListItem, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val image  = listImages[position]
        holder.bind(image)
    }

    override fun getItemCount(): Int {
        return listImages.size
    }

    inner class MyViewHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {


        fun bind(item:Uri ?) {
           itemView.findViewById<ImageView>(R.id.loadImg).setImageURI(item)

        }
    }
}