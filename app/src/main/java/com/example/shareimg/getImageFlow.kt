package com.example.shareimg

import android.content.Intent
import android.media.Image
import android.net.Uri
import android.os.Bundle
import android.os.Parcelable
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.shareimg.MyAdapter
import com.example.shareimg.listImages


class getImageFlow : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_image_flow)

        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)

        val layoutManager: RecyclerView.LayoutManager = GridLayoutManager(this, 2)

        recyclerView.layoutManager = layoutManager

//        recyclerView.setHasFixedSize(true)
        val myAdapter = MyAdapter()
        recyclerView.adapter = myAdapter
        handleSendMultipleImage(intent)
        handleSendImage(intent)

    }


        private fun handleSendImage(intent: Intent){
        (intent.getParcelableExtra<Parcelable>(Intent.EXTRA_STREAM) as? Uri)?.let {
            listImages.add(it)
        }
    }
    private fun handleSendMultipleImage(intent: Intent) {
        intent.getParcelableArrayListExtra<Parcelable>(Intent.EXTRA_STREAM)?.let {
            listImages = it.toMutableList() as MutableList<Uri>
        }
    }
}